import requests
import json
from datetime import datetime, timedelta

class WeatherForecast:
    def __init__(self, file_path='rain_forecast.json'):
        self.file_path = file_path
        self.data = self.load_data()

    def load_data(self):
        try:
            with open(self.file_path, 'r') as file:
                return json.load(file)
        except FileNotFoundError:
            return {}

    def save_data(self):
        with open(self.file_path, 'w') as file:
            json.dump(self.data, file, indent=4)

    def __getitem__(self, date):
        return self.data.get(date, "Nie wiem")

    def __setitem__(self, date, result):
        self.data[date] = result
        self.save_data()

    def __iter__(self):
        return iter(self.data)

    def items(self):
        return self.data.items()

    def check_rain_forecast(self, date):
        latitude = 53.0138  
        longitude = 18.5984
        api_url = f"https://api.open-meteo.com/v1/forecast?latitude={latitude}&longitude={longitude}&hourly=rain&daily=rain_sum&timezone=Europe%2FLondon&start_date={date}&end_date={date}"

        if not date:
            date = (datetime.now() + timedelta(days=1)).strftime('%Y-%m-%d') 

        if date in self.data:
            return self.data[date]

        response = requests.get(api_url)
        if response.status_code == 200:
            api_data = response.json()
            try:
                rain_sum = api_data['daily']['rain_sum'][0]
                if isinstance(rain_sum, (int, float)):
                    if rain_sum > 0.0:
                        result = "Będzie padać"
                    elif rain_sum == 0.0:
                        result = "Nie będzie padać"
                    else:
                        result = "Nie wiem"
                else:
                    result = "Nie wiem"
            except (KeyError, IndexError, TypeError):
                result = "Nie wiem"
        else:
            result = "Nie wiem"
        
        self[date] = result
        return result

if __name__ == "__main__":
    weather_forecast = WeatherForecast()
    search_date = input("Podaj datę w formacie YYYY-mm-dd: ").strip()
    print("Sprawdzanie prognozy pogody...")
    result = weather_forecast.check_rain_forecast(search_date)
    print(f"Prognoza pogody dla {search_date}: {result}")